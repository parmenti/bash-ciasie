#! /bin/bash

## Script de recherche sur la base de donnee
## Exemple d'utilisation : ./recherche.sh -r "Mathieu Kassovitz" -id

## Variables globales
TITRES=title.basics.tsv
NOMS=name.basics.tsv
REALISATEURS=title.crew.tsv
NOTES=title.ratings.tsv

## On verifie le nombre de parametre (qui doit etre egal a 2)
if [ $# -lt 2 ]
then
    echo "Usage : $0 type critere [id]"
    echo " avec type    au choix: [-r/-n/-t/-a/-e]"
    echo " et   critere une chaine de caracteres"
    exit 1
fi

type=$1
critere=$2
identifiant=$3

if [ "$identifiant" = "-id" ]
then
    champ="1"
else
    champ="3"
fi

#echo "type:"$type
#echo "critere:"$critere

case $type in
    "-t")
        #echo "Recherche sur le titre"
        #cat $TITRES | grep -i "$critere" | tr '\t' ';' | cut -d';' -f$champ

        # recherche "Amélie Poulain"
        # grep -i "Amélie" | grep -i "Poulain"
        
        # recherche "Amélie -Poulain"
        # grep -i "Amélie" | grep -i -v "Poulain"

        cmd="cat $TITRES"
        for crit in $critere
        do
            debut=$(echo $crit | cut -c1)
            if [ $debut = "-" ]
            then
                cmd=$cmd"| grep -i -v ${crit:1}"
            else
                cmd=$cmd"| grep -i $crit"
            fi
        done
        cmd=$cmd" | tr '\t' ';' | cut -d';' -f$champ"
        #echo $cmd
        eval "$cmd" #Pour executer la commande correspondant a la chaine cmd 
        ;;
    "-r")        
        #echo "Recherche sur le realisateur"
        #idrealisateurs=$(cat $NOMS | grep director | grep -i "$critere" | tr '\t' ';' | cut -d';' -f1)
        
        # Etape 1 : recuperer les identifiants des realisateurs de ce nom
        cmdreal="cat $NOMS | grep director "
        for crit in $critere
        do
            debut=$(echo $crit | cut -c1)
            if [ $debut = "-" ]
            then
                cmdreal=$cmdreal"| grep -i -v ${crit:1}"
            else
                cmdreal=$cmdreal"| grep -i $crit"
            fi
        done
        cmdreal=$cmdreal" | cut -f1"
        idrealisateurs=$(eval $cmdreal)
        
        # Etape 2 : pour chaque identifiant de realisateur
        for real in $idrealisateurs
        do
            # recuperer les identifiants de films correspondants
            idfilms=$(grep $real $REALISATEURS | cut -f1)
            # Etape 3 : pour chaque identifiant de film
            if [ $champ = 1 ]
            then
                echo $idfilms
            else
                for film in $idfilms
                do
                    # recuperer le titre correspondant
                    grep $film $TITRES | cut -f$champ
                done
            fi
        done
        ;;
    "-a")
        #echo "Recherche sur l'annee"

        # Ne suffit pas pour ne regarder que l'annee de creation
        # (selectionne aussi les oeuvres dont le titre contient l'annee)
        #grep "\s$critere\s" $TITRES | tr '\t' ';' | cut -d';' -f3
        
        # Etape 1 : recuperer les numeros de lignes correspondant aux
        # films recherches
        numlignes=$(cat $TITRES | cut -f6 | grep -n "$critere" | cut -d':' -f1)
        for numligne in $numlignes
        do
            cat $TITRES | head -$numligne | tail -1 | cut -f$champ
        done
        ;;
    "-n")
        #echo "Recherche sur la note"

        # Etape 1 : on recupere les identifiants de films ayant une note
        # assez grande par rapport au critere
        idnote=$(cat $NOTES | grep -v "averageRating" | tr '\t' ';' | cut -f1,2)
        # Pour chaque paire idfilm, note
        for film_note in $idnote
        do
            #echo "film_note : $film_note"
            idfilm=$(echo $film_note | cut -d ';' -f1)
            note=$(echo $film_note | cut -d';' -f2 | cut -d '.' -f1)
            #echo "identifiant film : $idfilm"
            #echo "note : $note"
            if [ "$note" -ge "$critere" ]
            then
                # Etape 2 : on extrait le titre associe a l'identifiant
                grep $idfilm $TITRES | cut -f$champ
            fi
        done
        ;;
    "-e") # Recherche par rapport au titre exact
        num_ligne=$(cat $TITRES | cut -f3 | grep -n "^$critere$" | cut -d':' -f1 | head -1) # en cas de films homonymes, on ne garde que le premier
        if [ -n "$num_ligne" ]
        then
            cat $TITRES | head -"$num_ligne" | tail -1 | cut -f$champ
        fi
        ;;
    *)
        echo "Type de recherche inconnu"
        exit 2
esac

#! /bin/bash

## Script principal offrant a l'utilisateur un menu textuel
## pour extraire des infos de la collection IMDb

menu()
{
    echo "****************************************************************"
    echo "* Bonjour. Que voulez vous faire ?                             *"
    echo "*                                                              *"
    echo "** 1 -> rechercher une oeuvre en fonction du titre             *"
    echo "** 2 -> rechercher une oeuvre en fonction du realisateur       *"
    echo "** 3 -> rechercher une oeuvre en fonction de son annee         *"
    echo "** 4 -> rechercher une oeuvre en fonction de sa note           *"
    echo "** 5 -> afficher la note d'une oeuvre en fonction de son titre *"
    echo "** 6 -> afficher les statistiques des recherchers              *"
    echo "** 7 -> quitter                                                *"
    echo "****************************************************************"
}

menu
echo ""
echo "Veuillez saisir votre choix ?"
read reponse

while [ "$reponse" != "7" ]
do
    case $reponse in
        "1") # Cas recherche par mots clés sur le titre
            echo -n "Merci de saisir vos criteres de recherche"
            echo -n "en fonction de mots clés du titre "
            echo -n "(en les faisant preceder par '-' en cas de "
            echo "critere d'exclusion) :"
            read criteres
            bash recherche.sh -t "$criteres" 
        ;;
        "2") # Cas recherche par realisateur
            echo -n "Merci de saisir vos criteres de recherche "
            echo -n "en fonction de realisateurs "
            echo -n "(en les faisant preceder par '-' en cas de "
            echo "critere d'exclusion) :"
            read criteres
            bash recherche.sh -r "$criteres"        
        ;;
        "3") # Cas recherche par annee
            echo -n "Merci de saisir l'annee correspondant "
            echo "a votre recherche :"
            read annee
            bash recherche.sh -a "$annee"        
        ;;
        "4") # Cas recherche par note
            echo -n "Merci de saisir le seuil (entier) au dessus duquel "
            echo "vous recherchez des films :"
            read note
            bash recherche.sh -n "$note"          
        ;;
        "5") # Cas affichage de note(s)
            echo -n "Merci de saisir le titre exact de l'oeuvre dont vous "
            echo "recherchez la note : "
            read titre
            nb_reponse=$(bash recherche.sh -e "$titre" -id | wc -l)
            if [ "$nb_reponse" -gt 1 ]
            then
                echo " * Attention : plusieurs films ont ce titre, on ne regarde que le 1er ici ! "
            elif [ "$nb_reponse" -eq 0 ]
            then
                echo "Film inconnu"
            fi
            resultat=$(bash recherche.sh -e "$titre" -id | ./getRating.sh | cut -d';' -f2) ##cut decoupe suivant ';' et prend la 2e colonne dans "titre;note"
            if [ -n "$resultat" ]
            then
                echo $resultat 
            else
                echo "note non disponible"
            fi
        ;;
        "6") # Cas affichage des statistiques
            bash stats.sh
        ;;
        *)
            echo "$reponse : choix inconnu"
            echo "merci de saisir un nombre entre 1 et 7"
        ;;
    esac

    menu
    echo ""
    echo "Veuillez saisir votre choix ?"
    read reponse
done

echo "Au revoir"
exit 0

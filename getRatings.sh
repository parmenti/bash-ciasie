#! /bin/bash

# Script recuperant la note d'un film dont l'identifiant est donnee
# sur l'entree standard

## Variables globales
TITRES=title.basics.tsv
NOMS=name.basics.tsv
REALISATEURS=title.crew.tsv
NOTES=title.ratings.tsv

## Recuperation de l'identifiant sur l'entree standard
# cf. https://stackoverflow.com/questions/20913635/how-to-read-multi-line-input-in-a-bash-script
idfilm=$(cat)
for f in $idfilm
do
    #echo "***********"
    #echo "* $f:" 
    titre=$(grep -E "^$f" $TITRES | cut -f3)
    note=$(grep -E "^$f" $NOTES | cut -f2)

    idreals=$(cat $REALISATEURS | grep $f | cut -f2)
    #echo "** $idreals"
    noms=""
    for real in $(echo $idreals | tr ',' ' ')
    do
        #echo "*** $real"
        nom_real=$(cat $NOMS | grep director | grep $real | cut -f2)
        noms="$nom_real/$noms"
    done
    
    resultat="$titre;$note"

    echo "$f:$resultat;$noms" >> $PWD/.imdb.log
    echo $resultat
done


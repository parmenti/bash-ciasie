# Mini projet Bash

## Auteurs : Yannick Parmentier et les étudiant.e.s de LP CIASIE - Groupe 1

Mini-projet réalisé dans le cadre du cours d'administration et outils système de la licence professionnelle CIASIE de du département informatique de l'IUT Nancy Charlemagne.

## Usages

- Recherche de titres / identifiants de films :
```bash
./recherche type criteres [-id] 
```
où 
- `type` est au choix : 
    - `-t` (critères appliqués au titre du film)
    - `-r` (critères appliqués aux réalisateurs du film)
    - `-a` (critère est l'année du film)
    - `-n` (critère : toutes les notes supérieures ou égales au seuil entier fourni)

- `criteres` est une chaîne de caractères.

- Recherche de notes :

```bash
./getRatings
```

Ce script attend sur son entrée standard une liste d'identifiants de films (et stocke les résultats dans le fichier ~/.imdb.log).

- Traitement des résultats de recherche :

```bash
./stats.sh
```

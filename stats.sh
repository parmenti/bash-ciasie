#! /bin/bash

# Script analysant le fichier de log ~/imdb.log pour en
# extraire des statistiques

## Variables globales
TITRES=title.basics.tsv
NOMS=name.basics.tsv
REALISATEURS=title.crew.tsv
NOTES=title.ratings.tsv
LOG=$PWD/.imdb.log
ANNEE=$(date +"%Y")

# Recuperation nombre de films recherches jusqu'a present
nb_film=$(cat $LOG | wc -l) #(a)
# Recuperation des realisateurs
realisateurs=""             #(b,e)
# Recuperation nombre de film cette annee
nb_film_annee=0             #(c)
# Recuperation moyenne des notes
somme_notes=0               #(d)
nb_notes=0

## Parcours du log
IFS=$'\n'
for f in $(cat $LOG)
do
    #echo "*$f"
    
    film=$(echo $f | cut -d';' -f1 | cut -d':' -f1)
    note=$(echo $f | cut -d';' -f2)
    real=$(echo $f | cut -d';' -f3)

    realisateurs="$real/$realisateurs"
    
    if [ -n "$note" ]
    then
        #echo "**$film -- **$note"
        valeur=$(echo "$note * 10" | bc | cut -d'.' -f1)
        #echo $valeur
        somme_notes=$(($somme_notes + $valeur))
        nb_notes=$(($nb_notes+1))
    fi

    annee_film=$(cat $TITRES | grep "$film" | tr '\t' ';' | cut -d';' -f6 | head -1)
    if [ "$annee_film" = $ANNEE ]
    then
        nb_film_annee=$(($nb_film_annee+1))
    fi
done

nb_notes=$(echo "$nb_notes * 10" | bc)
moyenne=$(echo "scale=2 ; $somme_notes / $nb_notes" | bc)

liste_real=$(echo $realisateurs | tr '/' '\n' | grep -v '^$' | sort -u | tr '\n' ',')
nb_real=$(echo $realisateurs | tr '/' '\n' | sort -u | wc -l)

echo "$nb_film $nb_real $nb_film_annee $moyenne $liste_real"
